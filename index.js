

	//Conditional statements allow us to control the flow of our program.

// [SECTION] If, Else if, and Else Statement

	let numA= -1;
	//[SUB-SECTION] If statement

		if(numA < 0){
			console.log('Hellow');
		};

	/*
	 Syntax:
	
	 if(condition){
		statement
	 };

	*/

	console.log(numA < 0);

	numA= 0

	if (numA < 0){
		console.log('Hello again if numA is 0!');
	};

	console.log(numA < 0);

	//Another example:

		let city= 'New York';

		if(city === 'New York'){
			console.log('Welcome to New York City!');
		};


// [SUB SECTION] Else if Statement
		
		let numH= 1;
		if (numA < 0){
			console.log('Hellow!');
		} else if (numH > 1){
			console.log('World');
		} else if (numH > 0){
			console.log('YES');
		};

	// We were able to run the else if() statement after we evaluated
	// that the if  condition was failed


		numA= 1

		if(numA > 0){
			console.log('Another Hello')
		} else if(numH > 0){
			console.log('Another World')
		};

		//Let's another one

		city= 'Tokyo';

		if(city === 'New York'){
			console.log('Welcome to New York City!')
		}else if( city === 'Tokyo'){
			console.log('Welcome to Tokyo, Japan')
		}else if (city === 'Manila'){
			console.log('Welcome to Manila City!')
		};
	//Since we failed the condition for the first if(), we went
	//to the else if() and checked and instead passed that condition.\

//SUB SECTION] Else statement
		
		if(numA<0){
			console.log('H3llow');
		} else if (numH === 0){
			console.log('W0rld');
		} else {
			console.log('Again, again')
		};

	// Else statement should only be added if there is a preceding if
	//condition. Else statement by itself will not work, however,
	//if statements will work even if there is no else statement.


//[SUB SECTION] If, Else if, and Else Statements with Functions

		let message= 'No Message.';
		console.log(message);

		function determineTyphoonIntensity(windSpeed){
			if(windSpeed < 30){
				return 'Not a typhoon yet.';
			} else if(windSpeed <= 61){
				return 'Tropical depression detected.';
			} else if(windSpeed>= 62 && windSpeed <= 88){
				return 'Tropical storm detected.'
			} else if (windSpeed >= 89 && windSpeed <= 117){
				return 'Severe tropical storm detected.';
			} else {
				return 'Typhoon detected'
			};
		};

		message= determineTyphoonIntensity(63)
		console.log(message);

		// - We can further  control the flow of our program based
		//on conditions and changing variables and results.

		if(message == 'Tropical storm detected.'){
			console.warn(message);
		};

		// [SECTION] Truthy and Falsy
		// in Javascript, a truthy value is a value that is considired
		// true when encountered in a Boolean context

			/*
			-Falsy Values/Exception for truthy:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN
			
			*/

		//SUBSECTION - Truthy Example

			if(true){
				console.log('Truthy')
			};

			if(1){
				console.log('Truthy')
			};

			if ([]){
				console.log('Truthy')
			};

		//SubSection - Falsy

			if(false){
				console.log('Falsy')
			};

			if(0){
				console.log('Falsy')
			};

			if(undefined){
				console.log('Falsy')
			};

		// [section] conditional ternary operator
		/*
			the conditional ternary operator takes in 3 operands
			1.condition
			2 .experession to excute if the conditon is truty
			3. dafsdfasd execuf if cdiiton is falsy
			syntax:
			(expression) ? ifTrue : ifFalse;
		*/

			//signle statement executuion

				let ternaryResult= (1<18) ? true : false ;
				console.log('RESULT OF TERNARY OPERATOR: '+ternaryResult);

			// multiple statement execution
				let name;

				function isOfLegalAge(){
					name= 'John';
					return 'You are of the legal age limit'
				};

				function isUnderAge(){
					name= 'Jane';
					return 'You are under the age limit';
				};
					/*
						- The 'parseInt' function converts the input received
						into a number data type.

					*/

				let age= parseInt(prompt('What is your age'));
				console.log(age);

				let legalAge= (age > 18) ? isOfLegalAge() : isUnderAge();
				console.log('Result of Ternary Operator in functions: '+legalAge+ ", "+name);

			//SECTION - Switch Statement

				//- the switch statement evalutes an expressio and matches the expression's
				//value to a case clause. The switch willl then executes the statemetns aassosciated
				//with the case, as well as statement in cases that follow
				//the matching cases.

				//the ".toLowerCase()" function/method will change the input received from the prompt
				//into all lower case letters ensuring match with the switch case condiition
				//if the user inputs capitalized or uppercased letters.

					/*
						Syntax:
						switch (expression){
							case value:
							statement:
							break:
						default:
							statement:
							break:

						}

		
					*/

				let day= prompt('What day of the week is it today?').toLowerCase();
				console.log(day);

					switch(day){
						case 'monday':
							console.log('The color of the day is Red')
							break;
						case 'tuesday':
							console.log('The color of the day is Orange')
							break;
						case 'wednesday':
							console.log('The color of the day is Yellow')
							break;
						case 'thursday':
							console.log('The color of the day is Green')
							break;
						case 'friday':
							console.log('The color of the day is Blue')
							break;

						case 'saturday':
							console.log('The color of the day is Indigo')
							break;

						case 'sunday':
							console.log('The color of the day is Violet')
							break;
						default:
							console.warn('Please input a valid day!')
							break;
					};

			// Try-Catch-Finally Statement

				//'try catch' statements are commonly used for error handling

					//example of trycathcfinnaly

/*					function showIntesnsityAlert(windSpeed){
						try{
							console.log('No Error Here');
							alerat(determineTyphoonIntensity(windSpeed));
						} catch (error){
							//error message is used to acces the info to an error object
							console.log(typeof error);
							console.warn(error.message);
						} finally {

							//continue execution of code regardless of suces n failure of code
							execution in the try block to handle resolve errors
							alert('Intesnsity updates will show new alert.')
							console.log('Hellowww')	
						}
					}

					showIntesnsityAlert(67)
*/					

